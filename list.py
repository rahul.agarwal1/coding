class SortedList(list):
  def append(self, value):
    super().append(value)
    self.sort()
  
a_sorted_list = SortedList([5, 9, 7, 3])

a_sorted_list.append(4)
print(a_sorted_list)
